# The Book

'JavaScript Web Applications'
by Alex MacCaw

## TODO

Take a look:
NODE:
C:\Projects\js.projects.pub-learn.jasmine-node

BROWSER:
C:\Projects\vz\VMS.API.Tester\svg\test




## Project structure

The project structure is:

	README.md           this file
	node_modules        contains node modules relevant for the project
	run-tests.bat       runs the unit tests
	code\src			contains the source code
	code\deps			contains the dependencies of the code
	code\tests          contains the unit tests


## Installation

### NodeJS modules

This project uses 'mocha' and 'expect' for unit testing.
It also uses 'underscore' for the unit testing.

Install 'mocha' node module:

	npm install -g mocha


Setup of git dependencies has been done:

	git submodule add git://github.com/documentcloud/underscore.git node_modules/underscore

	git submodule add git://github.com/visionmedia/mocha.git node_modules/mocha
	git submodule add git://github.com/LearnBoost/expect.js.git node_modules/expect

	git submodule add git://github.com/mhevery/jasmine-node.git node_modules/jasmine
	git submodule add git://github.com/larrymyers/jasmine-reporters.git node_modules/jasmine-reporters
	git submodule add git://github.com/substack/node-findit.git node_modules/findit
	git submodule add git://github.com/substack/node-seq.git node_modules/seq
	git submodule add git://github.com/substack/node-hashish.git node_modules/hashish
	git submodule add git://github.com/substack/js-traverse.git node_modules/traverse
	git submodule add git://github.com/substack/node-chainsaw.git node_modules/chainsaw


When cloning the repository, download the needed dependencies:

	git submodule init
	git submodule update

Note: To remove a submodule from git repository:

- Delete the relevant line from the .gitmodules file.
- Delete the relevant section from .git/config.
- Run git rm --cached path_to_submodule (no trailing slash).

Example:

	git rm --cached node_modules/nodeunit



## Unit testing

### Jasmine-Node

You can now run `jasmine-node` from the project's home directory:

	node node_modules\jasmine\bin\jasmine-node

	USAGE: jasmine-node [--color|--noColor] [--verbose] [--coffee] directory

	Options:
	  --autotest         - rerun automatically the specs when a file changes
	  --color            - use color coding for output
	  --noColor          - do not use color coding for output
	  -m, --match REGEXP - load only specs containing "REGEXPspec"
	  --verbose          - print extra information per each test run
	  --coffee           - load coffee-script which allows execution .coffee files
	  --junitreport      - export tests results as junitreport xml format
	  --teamcity         - converts all console output to teamcity custom test runner commands. (Normally auto detected.)
	  --runWithRequireJs - loads all specs using requirejs instead of node's native require method
	  --test-dir         - the absolute root directory path where tests are located
	  --nohelpers        - does not load helpers.
	  -h, --help         - display this help and exit


To run the tests from the command prompt run `run-tests.bat`:

	$ run-tests.bat

	  Base
		Namespacing
		  JsWebApp
			✓ should be defined
			✓ should be object
		  JsWebAppBase
			✓ should be defined
			✓ should be object
		  JsWebAppBase.Class
			✓ should be defined
			✓ should be function
		Inheritance
		  JsWebAppBase.Class
			property 'extend'
			  ✓ is defined
			  ✓ is method
			  ✓ can receive 'extended' callback
			  ✓ can add class methods
			property 'include'
			  ✓ is defined
			  ✓ is method
			  ✓ can receive 'included' callback
			  ✓ can add object methods


	  ✔ 14 tests complete (40ms)


