//noinspection JSUnresolvedFunction
describe('Base', function(){

	beforeEach(function(){

		if (require) {
			_ = require('underscore');
			JsWebAppBase = require('../src/base.js').JsWebAppBase;
			JsWebAppUtils = require('../src/base.utils.js').JsWebAppUtils;
		}

	});

	afterEach(function() {
	});


	describe("Namespacing", function() {

		describe("JsWebAppBase", function() {
			it('should be defined', function() {
				expect(JsWebAppBase).toBeDefined();
			});
			it('should be object', function() {
				expect(JsWebAppBase).toEqual('object');
			});
		});

		describe("JsWebAppBase.Class", function() {
			it('should be defined', function() {
				expect(JsWebAppBase.Class).toBeDefined();
			});
			it('should be object', function() {
				expect(JsWebAppBase.Class).toEqual('object');
			});
		});

	});

	describe("Instantiation", function() {
	
		describe("when JsWebAppBase.Class called without 'new'", function() {
		
			it("will run 'new' internally", function() {

				var Apple = JsWebAppBase.Class();

				expect(_.has(Apple, 'extend')).toBeTruthy();
				expect(_.has(Apple, 'include')).toBeTruthy();

			});
		});
	});

	describe("Inheritance", function() {
	
		describe("JsWebAppBase.Class", function() {

			describe("property 'extend'", function() {

				it("is defined", function() {
					var Database = new JsWebAppBase.Class();
					expect(_.has(Database, "extend")).toBeTruthy();
				});

				it("is method", function() {
					var Database = new JsWebAppBase.Class();
					expect(_.isFunction(Database.extend)).toBeTruthy();
				});

				it("can receive 'extended' callback", function() {
					var _ = this.underscore;
					var Class = this.sut.JsWebAppBase.Class;

					var extended = false;

					var Database = new Class();
					Database.extend({
						extended: function(klass) {
							extended = true;
						}
					});

					this.expect(extended).to.be.ok();
				});

				it("can add class methods", function() {
					var _ = this.underscore;
					var Class = this.sut.JsWebAppBase.Class;

					var extended = false;

					var Database = new Class();
					Database.extend({
						countOfTables: function() {
							return 123;
						}
					});

					this.expect(Database.countOfTables()).to.be(123);
				});

			});


			describe("property 'include'", function() {

				it("is defined", function() {
					var _ = this.underscore;
					var Class = this.sut.JsWebAppBase.Class;

					var Animal = new Class();

					this.expect(_.has(Animal, "include")).to.be.ok();
				});

				it("is method", function() {
					var _ = this.underscore;
					var Class = this.sut.JsWebAppBase.Class;

					var Animal = new Class();

					this.expect(_.isFunction(Animal.include)).to.be.ok();
				});

				it("can receive 'included' callback", function() {
					var _ = this.underscore;
					var Class = this.sut.JsWebAppBase.Class;

					var included = false;

					var Animal = new Class();
					Animal.include({
						included: function(klass) {
							included = true;
						}
					});

					this.expect(included).to.be.ok();
				});

				it("can add object methods", function() {
					var _ = this.underscore;
					var Class = this.sut.JsWebAppBase.Class;

					var Animal = new Class();
					Animal.include({
						canBreath: function() {
							return true;
						}
					});

					var Cat = new Class(Animal);
					var cat = new Cat();

					this.expect(cat.canBreath()).to.be.ok();
				});

			});
		});
		
		
	});


	describe("Proxying", function() {

		describe("JsWebAppBase.Class.proxy", function() {

			it("can keep class' scope", function() {

				var _ = this.underscore;
				var Class = this.sut.JsWebAppBase.Class;

				var Button = new Class();

				Button.include({

					init: function(element){
						this.element = element;

						// Proxy the click function
						this.element.onclick = this.proxy(this.click);
					},

					wasClicked: false,

					click: function() {
						this.wasClicked = true;
					}
				});

			});
		});
	});



});

