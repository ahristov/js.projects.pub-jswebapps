/**
 * Created by JetBrains WebStorm.
 * User: ahristov
 * Date: 3/24/12
 * Time: 9:28 AM
 * To change this template use File | Settings | File Templates.
 */

var JsWebAppBase = {};


JsWebAppBase.Class = function(parent) {

	if (this.__proto__.constructor != JsWebAppBase.Class) {
		return new JsWebAppBase.Class(parent);
	}

	var klass = function() {
		this.init.apply(this, arguments);
	};
		
	// Change Klass' prototype
	if (parent) {
		var subclass = function() {};
		subclass.prototype = parent.prototype;
		klass.prototype = new subclass();
	}

	klass.prototype.init = function() {};

	// Shortcuts
	klass.fn = klass.prototype;
	klass.fn.parent = klass;
	klass._super = klass.__proto__;


	// Add class properties
	klass.extend = function(obj) {
		for (var i in obj) { klass[i] = obj[i]; }
		obj.extended && obj.extended(klass);
	};

	// Add instance properties
	klass.include = function(obj) {
		for (var i in obj) { klass.fn[i] = obj[i]; }
		obj.included && obj.included(obj);
	};

	// Add proxy function
	klass.proxy = function(func){
		var self = this;
		return(function(){
			return func.apply(self, arguments);
		});
	}

	return klass;
};

exports && (exports.JsWebAppBase = JsWebAppBase);