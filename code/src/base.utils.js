/**
 * Created by JetBrains WebStorm.
 * User: ahristov
 * Date: 3/31/12
 * Time: 12:35 PM
 * To change this template use File | Settings | File Templates.
 */

var JsWebAppUtils = {};

JsWebAppUtils = (function() {

	/**
	 * Dumps object to string.
	 * @param arr
	 * @param level
	 */
	function dump(arr, level) {
		var dumped_text = "";
		if(!level) level = 0;

		var level_padding = "";
		for(var j=0;j<level+1;j++) level_padding += "    ";

		if(typeof(arr) == 'object') {
			for(var item in arr) {
				var value = arr[item];

				if(typeof(value) == 'object') {
					dumped_text += level_padding + "'" + item + "' ...\n";
					dumped_text += dump(value,level+1);
				} else {
					dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
				}
			}
		} else {
			dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
		}
		return dumped_text;
	}


	return {
		dump: dump
	};

}());

exports && (exports.JsWebAppUtils = JsWebAppUtils);